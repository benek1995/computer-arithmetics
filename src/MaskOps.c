#include "interface.h"

bitword input = 0x00;       // 16-bit input binary word
bitword mask = 0x00;        // 16-bit mask binary word
bitword output = 0x00;      // 16-bit output binary word

void onSetMasked() {
    // TODO: Implement this function
    // set each output bit to 1 if corresponding bit in the mask is set,
    // otherwise copy the original input bit unchanged
}

void onClearMasked() {
    // TODO: Implement this function
    // clear each output bit to 0 if corresponding bit in the mask is set,
    // otherwise copy the original input bit unchanged
}

void onFlipMasked() {
    // TODO: Implement this function
    // set each output bit to the negation of input bit if corresponding bit
    // in the mask is set, otherwise copy the original input bit unchanged
}

void onGetMasked() {
    // TODO: Implement this function
    // set each output bit to the original input bit if corresponding bit
    // in the mask is set, otherwise clear the output bit
}

void onSetUnmasked() {
    // TODO: Implement this function
    // set each output bit to 1 if corresponding bit in the mask is cleared,
    // otherwise copy the original input bit unchanged
}

void onClearUnmasked() {
    // TODO: Implement this function
    // clear each output bit to 0 if corresponding bit in the mask is cleared,
    // otherwise copy the original input bit unchanged
}

void onFlipUnmasked() {
    // TODO: Implement this function
    // set each output bit to the negation of input bit if corresponding bit
    // in the mask is cleared, otherwise copy the original input bit unchanged
}

void onGetUnmasked() {
    // TODO: Implement this function
    // set each output bit to the original input bit if corresponding bit
    // in the mask is cleared, otherwise clear the output bit
}
