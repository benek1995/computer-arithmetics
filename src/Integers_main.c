#include <stdlib.h>
#include <stdio.h>
#include "interface.h"

enum {
    BITS_COUNT = 16,
    MAX_TEXT_LEN = 30
};

typedef unsigned char bit;

extern bit valueBits[BITS_COUNT];
extern bit deltaBits[BITS_COUNT];

bitword deltaWord = 0;
bitword valueWord = 0;
bitword selection = 0;

void fillBitArrays() {
    int bit;
    for (bit = 0; bit < BITS_COUNT; bit++) {
        valueBits[bit] = (valueWord & 1<<bit) ? 1 : 0;
        deltaBits[bit] = (deltaWord & 1<<bit) ? 1 : 0;
    }
}

void fillBitWords() {
    valueWord = 0;
    deltaWord = 0;
    int bit;
    for (bit = 0; bit < BITS_COUNT; bit++) {
        valueWord |= valueBits[bit] << bit;
        deltaWord |= deltaBits[bit] << bit;
    }
}

void onDeltaClick(int bit) {
    deltaWord ^= 1 << bit;
    fillBitArrays();
}

void onValueClick(int bit) {
    valueWord ^= 1 << bit;
    fillBitArrays();
}

char unsignedText[MAX_TEXT_LEN];
char signedText[MAX_TEXT_LEN];

unsigned int getUnsignedValue();
signed int getSignedValue();

char* getUnsignedText() {
    snprintf(unsignedText, MAX_TEXT_LEN, "%26u", getUnsignedValue());
    return unsignedText;
}

char* getSignedText() {
    snprintf(signedText, MAX_TEXT_LEN, "%+26d", getSignedValue());
    return signedText;
}

void onIncrement();
void onDecrement();
void onAddDelta();
void onSubDelta();

void onIncrementUI() {
    onIncrement();
    fillBitWords();
}

void onDecrementUI() {
    onDecrement();
    fillBitWords();
}

void onAddDeltaUI() {
    onAddDelta();
    fillBitWords();
}

void onSubDeltaUI() {
    onSubDelta();
    fillBitWords();
}

int main() {
    if (!initInterface()) {
        return EXIT_FAILURE;
    }

    BINARYWINDOW deltaWin, valueWin;
    newBinaryWin(&deltaWin, 3, 0, "Delta", COLOR_BLUE, BITS_COUNT, &deltaWord, &selection, onDeltaClick);
    newBinaryWin(&valueWin, 10, 0, "Value", COLOR_WHITE, BITS_COUNT, &valueWord, &selection, onValueClick);

    COMMANDWINDOW cmd[4];
    COMMANDWINDOW* cptr = cmd;
    newCommandWin(cptr++, 3, 66, 13, "ADD delta", COLOR_MAGENTA, onAddDeltaUI);
    newCommandWin(cptr++, 6, 66, 13, "SUB delta", COLOR_MAGENTA, onSubDeltaUI);
    newCommandWin(cptr++, 10, 66, 13, "INC", COLOR_CYAN, onIncrementUI);
    newCommandWin(cptr++, 13, 66, 13, "DEC", COLOR_CYAN, onDecrementUI);

    OUTPUTWINDOW out[8];
    OUTPUTWINDOW* optr = out;
    newOutputWin(optr++, 18, 2, 30, "Unsigned decimal", COLOR_YELLOW, getUnsignedText);
    newOutputWin(optr++, 18, 34, 30, "Signed decimal", COLOR_GREEN, getSignedText);

    eventLoop();
    closeInterface();
    return EXIT_SUCCESS;
}

